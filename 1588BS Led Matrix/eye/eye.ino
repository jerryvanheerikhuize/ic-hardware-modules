/*#################################################################################*\
 
     ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
    ▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
    ▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
    ░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
    ░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
     ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
     ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
     ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
     ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
                                               ░                              ░ 
    -----------------------------------------------------------------------------

    Developed by:			      Jerry van Heerikhuize
    Modified by:            Jerry van Heerikhuize

    -----------------------------------------------------------------------------

    Version:                1.0.0
    Creation Date:          23/09/18
    Modification Date:      23/09/18
    Email:                  hello@halfbros.nl
    Description:            Animates an eye
    File:                   eyeI.ino

\*#################################################################################*/

#include "eyes.h"


// Pinout
int _Rows[] = { 2, 7, A3, 5, 10, A2, 11, A0 };
int _Cols[] = { 6, 12, 13, 3, A1, 4, 8, 9 };    

// Serial setup
const byte numChars = 32;
char receivedChars[numChars];
boolean newData = false;

// 
boolean opened = false;



int fpsDelay = 3; // 1 = continueous, 2 = vhs, 3 = retro


void setup() {
  
  Serial.begin (9600);
  while (!Serial) {};

  for(int i = 0; i<8; i++) {
    pinMode(_Rows[i],OUTPUT);  
    pinMode(_Cols[i],OUTPUT);  
  }

  Serial.println("TRINITY_T1:EYEI_T1:Setup:success");
  
  splash();

}

void loop() {

  render(eye);

  recvWithStartEndMarkers();
  showNewData();
}

void reset() {
  for(int i = 0; i < 8; i++) {  
    digitalWrite(_Rows[i], LOW);  
    digitalWrite(_Cols[i], HIGH);  
  }
}

void render(unsigned char data[8][8]){
  for(int c = 0; c<8; c++) {  
    digitalWrite(_Cols[c],LOW);
    
    for(int r = 0; r<8; r++) {  
      digitalWrite(_Rows[r],data[r][c]);  
    }
    delay(fpsDelay);  
    reset();
  }
}  



void recvWithStartEndMarkers() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;
 
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}

void showNewData() {
    if (newData == true) {
        serialHandler(receivedChars);
        newData = false;
    }
}


void serialHandler (String command){
    Serial.print("TRINITY_T1:EYEI_T1:serialHandler:Received command:");
    Serial.println(command);

    int startmarkerI = command.indexOf('<');
    String startmarker = command.substring(0, startmarkerI);
    int actionI = command.indexOf(':');
    String action = command.substring(startmarkerI+1, actionI);
    int targetI = command.indexOf(':', actionI+1 );
    String target = command.substring(actionI+1, targetI);

    SDK(action, target, "");

}


void SDK (String action, String target, String arguments){
    if (action == "exe"){
        if (target == "open"){
            open();
        } else if (target == "blink"){
            blink();
        } else if (target == "love"){
            love();
        } else {
            Serial.println("TRINITY_T1:EYEI_T1:SDK:Target not found");
        }
    } else {
        Serial.println("TRINITY_T1:EYEI_T1:SDK:Action not found");
    }
}


void splash() {

    for(int i = 0 ; i < (20/fpsDelay) ; i++) {  
        render(on);
    }

    for(int i = 0 ; i < (20/fpsDelay) ; i++) {  
        render(even);
    }

    for(int i = 0 ; i < (20/fpsDelay) ; i++) {  
        render(uneven);
    }
    
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(off);
    }  
  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(splash_1);
    }  

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(splash_2);
    }  

    for(int i = 0 ; i < (250/fpsDelay) ; i++) {     
        render(splash_3);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(splash_2);
    } 

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(splash_1);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(off);
    }
    
    Serial.println("TRINITY_T1:EYEI_T1:splash:success");

    open();
}


void open() {

    for(int i = 0 ; i < (20/fpsDelay) ; i++) {  
        render(on);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye_close2);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye_close1);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    Serial.println("TRINITY_T1:EYEI_T1:open:success");
}



void blink() {  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye_close1);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye_close2);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    Serial.println("TRINITY_T1:EYEI_T1:blink:success");
}





void love() {  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    for(int i = 0 ; i < (200/fpsDelay) ; i++) {     
        render(heart);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }
    Serial.println("TRINITY_T1:EYEI_T1:love:success");
}




void interference_1_a() {  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(interference_1);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }
    Serial.println("TRINITY_T1:EYEI_T1:interference_1_a:success");
}




void interference_2_a() {  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(interference_2);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }
    Serial.println("TRINITY_T1:EYEI_T1:interference_2_a:success");
}



void interference_3_a() {  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(interference_3);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }
    Serial.println("TRINITY_T1:EYEI_T1:interference_3_a:success");
}



void interference_4_a() {  
    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(interference_4);
    }

    for(int i = 0 ; i < (10/fpsDelay) ; i++) {     
        render(eye);
    }
    Serial.println("TRINITY_T1:EYEI_T1:interference_4_a:success");
}


