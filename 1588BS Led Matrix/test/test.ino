const int Rows[8] = { 2, 7, A3, 5, 10, A2, 11, A0 };
const int Columns[8] = { 6, 12, 13, 3, A1, 4, 8, 9 };

int pixels[8][8];

int posX = 7;
int posY = 7;
int count = 30;
bool bg = false;

void setup() {

  for (int thisPin = 0; thisPin < 8; thisPin++) {

    pinMode(Columns[thisPin], OUTPUT);
    pinMode(Rows[thisPin], OUTPUT);
    digitalWrite(Columns[thisPin], HIGH);
  
  }

  setupScreen();

}

void loop() {

  refreshScreen();

  if(count-- == 0){
    count = 500;
    if(posX--==0){
      posX = 7;
      if(posY--==0){
        posY = 7;
        bg = !bg;
      }
    }
    setupScreen();
  }
}

void setupScreen(){
  if(bg){
    for (int x = 0; x < 8; x++) {
      for (int y = 0; y < 8; y++) {
        pixels[x][y] = LOW;
      }
    }
    pixels[posX][posY] = HIGH;
  }else{
    for (int x = 0; x < 8; x++) {
      for (int y = 0; y < 8; y++) {
        pixels[x][y] = HIGH;
      }
    }
    pixels[posX][posY] = LOW;
  }
}

void refreshScreen() {
  for (int thisRow = 0; thisRow < 8; thisRow++) {
    digitalWrite(Rows[thisRow], HIGH);
    for (int thisCol = 0; thisCol < 8; thisCol++) {
      int thisPixel = pixels[thisRow][thisCol];
      digitalWrite(Columns[thisCol], thisPixel);
      if (thisPixel == LOW) {
        digitalWrite(Columns[thisCol], HIGH);
      }
    }
    digitalWrite(Rows[thisRow], LOW);
  }
}
