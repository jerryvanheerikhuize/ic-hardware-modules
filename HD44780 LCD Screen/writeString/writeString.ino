/*#################################################################################*\
 
   ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
  ▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
  ▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
  ░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
  ░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
   ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
   ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
   ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
   ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
                         ░                              ░ 
  -----------------------------------------------------------------------------

  Developed by:     Jerry van Heerikhuize
  Modified by:            Jerry van Heerikhuize

  -----------------------------------------------------------------------------

  Version:                1.0.0
  Creation Date:          11/09/18
  Modification Date:      11/09/18
  Email:                  hello@halfbros.nl
  Description:            Output a string to the LCD screen
  File:                   readSendor.ino

\*#################################################################################*/

#include <LiquidCrystal.h>

#define CHARACTERS 16
#define ROWS 2

#define CONTRAST_PIN 6
#define CONTRAST 100
#define BACKLIGHT_PIN 9
#define BACKLIGHT 5000
#define CS_PIN 10

#define STR "Hello world!"

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
    lcd.begin(CHARACTERS, ROWS);

    analogWrite(CONTRAST_PIN, CONTRAST);
    analogWrite(BACKLIGHT_PIN, BACKLIGHT);
    lcd.setCursor(2, 0);
    lcd.print(STR);
}

void loop() {
    lcd.setCursor(2, 1);
    
    // print the number of seconds since reset:
    lcd.print(millis() / 1000);
}
