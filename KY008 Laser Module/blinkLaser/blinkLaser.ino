/*#################################################################################*\
 
   ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
  ▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
  ▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
  ░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
  ░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
   ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
   ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
   ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
   ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
                         ░                              ░ 
  -----------------------------------------------------------------------------

  Developed by:     Jerry van Heerikhuize
  Modified by:            Jerry van Heerikhuize

  -----------------------------------------------------------------------------

  Version:                1.0.0
  Creation Date:          11/09/18
  Modification Date:      11/09/18
  Email:                  hello@halfbros.nl
  Description:            Read the sensor and blink a led on activity
  File:                   readSendor.ino

\*#################################################################################*/

#define VCC_INPUT_PIN     A0
#define LASER_OUTPUT_PIN  2
#define LED_OUTPUT_PIN    13

int voltage = 0;

void setup() {  
  Serial.begin(9600); // starting the USB serial interface and setting the baud rate (transmission speed) to 9600
  pinMode (LASER_OUTPUT_PIN, OUTPUT);
  pinMode (LED_OUTPUT_PIN, OUTPUT);
  
  digitalWrite(LASER_OUTPUT_PIN, LOW);
  digitalWrite(LED_OUTPUT_PIN, LOW);
}

void loop() {
  digitalWrite(LASER_OUTPUT_PIN, HIGH);
  digitalWrite(LED_OUTPUT_PIN, HIGH); 
  
  voltage = analogRead(VCC_INPUT_PIN);
  Serial.print("the laser is ON and the voltage on the center pin is ");
  Serial.println(voltage * (5.0 / 1023.0));
  delay(1000);

  digitalWrite(LASER_OUTPUT_PIN, LOW);
  digitalWrite(LED_OUTPUT_PIN, LOW);
  
  voltage = analogRead(VCC_INPUT_PIN);
  Serial.print("the laser is OFF and the voltage on the center pin is ");
  Serial.println(voltage * (5.0 / 1023.0));
  delay(1000);
}
