/*#################################################################################*\
 
   ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
  ▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
  ▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
  ░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
  ░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
   ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
   ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
   ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
   ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
                         ░                              ░ 
  -----------------------------------------------------------------------------

  Developed by:           Jerry van Heerikhuize
  Modified by:            Jerry van Heerikhuize

  -----------------------------------------------------------------------------

  Version:                1.0.0
  Creation Date:          11/09/18
  Modification Date:      11/09/18
  Email:                  hello@halfbros.nl
  Description:            Detect pushbutton pushes
  File:                   pushButton.ino

\*#################################################################################*/

#define INPUT_PIN   7
#define OUTPUT_PIN  13

int buttonPushCounter = 0;
int buttonState = 0;
int lastButtonState = 0;

void setup() {
    Serial.begin(9600);
    pinMode(INPUT_PIN, INPUT);
    pinMode(OUTPUT_PIN, OUTPUT);
}


void loop() {

    buttonState = digitalRead(INPUT_PIN);

    if (buttonState != lastButtonState) {

        if (buttonState == HIGH) {
            buttonPushCounter++;
            Serial.println("on");
            Serial.print("number of button pushes: ");
            Serial.println(buttonPushCounter);
        } else {
            Serial.println("off");
        }
        delay(50); // Delay a little bit to avoid bouncing
    }

    lastButtonState = buttonState;

    if (buttonPushCounter % 2 == 0) {
        digitalWrite(OUTPUT_PIN, HIGH);
    } else {
        digitalWrite(OUTPUT_PIN, LOW);
    }
}
