/*#################################################################################*\
 
	 ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
	▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
	▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
	░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
	░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
	 ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
	 ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
	 ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
	 ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
											   ░                              ░ 
	-----------------------------------------------------------------------------

	Developed by:			Jerry van Heerikhuize
	Modified by:            Jerry van Heerikhuize

	-----------------------------------------------------------------------------

	Version:                1.0.0
	Creation Date:          11/09/18
	Modification Date:      11/09/18
	Email:                  hello@halfbros.nl
	Description:            Read the sensor and blink a led on activity
	File:                   readSendor.ino

\*#################################################################################*/

#define INPUT_PIN   2
#define OUTPUT_PIN  13

void setup() {
    Serial.begin(9600); 
    pinMode (INPUT_PIN, INPUT);
    pinMode (OUTPUT_PIN, OUTPUT);
}

void loop() {
    int val = digitalRead(INPUT_PIN);
    if (val == HIGH){
        digitalWrite(OUTPUT_PIN, HIGH);
    } else {
        digitalWrite(OUTPUT_PIN, LOW);
    }
}

