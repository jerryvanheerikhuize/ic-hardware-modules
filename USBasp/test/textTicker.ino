/*#################################################################################*\
 
     ██░ ██  ▄▄▄       ██▓      █████▒    ▄▄▄▄    ██▀███   ▒█████    ██████      
    ▓██░ ██▒▒████▄    ▓██▒    ▓██   ▒    ▓█████▄ ▓██ ▒ ██▒▒██▒  ██▒▒██    ▒      
    ▒██▀▀██░▒██  ▀█▄  ▒██░    ▒████ ░    ▒██▒ ▄██▓██ ░▄█ ▒▒██░  ██▒░ ▓██▄        
    ░▓█ ░██ ░██▄▄▄▄██ ▒██░    ░▓█▒  ░    ▒██░█▀  ▒██▀▀█▄  ▒██   ██░  ▒   ██▒     
    ░▓█▒░██▓ ▓█   ▓██▒░██████▒░▒█░       ░▓█  ▀█▓░██▓ ▒██▒░ ████▓▒░▒██████▒▒ ██▓ 
     ▒ ░░▒░▒ ▒▒   ▓▒█░░ ▒░▓  ░ ▒ ░       ░▒▓███▀▒░ ▒▓ ░▒▓░░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░ ▒▓▒ 
     ▒ ░▒░ ░  ▒   ▒▒ ░░ ░ ▒  ░ ░         ▒░▒   ░   ░▒ ░ ▒░  ░ ▒ ▒░ ░ ░▒  ░ ░ ░▒  
     ░  ░░ ░  ░   ▒     ░ ░    ░ ░        ░    ░   ░░   ░ ░ ░ ░ ▒  ░  ░  ░   ░   
     ░  ░  ░      ░  ░    ░  ░            ░         ░         ░ ░        ░    ░  
                                               ░                              ░ 
    -----------------------------------------------------------------------------

    Developed by:			Jerry van Heerikhuize
    Modified by:            Jerry van Heerikhuize

    -----------------------------------------------------------------------------

    Version:                1.0.0
    Creation Date:          11/09/18
    Modification Date:      11/09/18
    Email:                  hello@halfbros.nl
    Description:            Output a string to the led matrix and scroll it
    File:                   textTicker.ino

\*#################################################################################*/

#include <MD_Parola.h>

// Configure Module Pins
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4
#define CLK_PIN   13
#define DATA_PIN  11
#define CS_PIN    10

#define MSG "Fijne dag vandaag lieve popjes. You rock my wold! xxx"

// Create Parola instance
MD_Parola P = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

uint8_t scrollSpeed = 30;
textEffect_t scrollEffect = PA_SCROLL_LEFT;
textPosition_t scrollAlign = PA_LEFT;
uint16_t scrollPause = 2000;

// Setup message
#define	BUF_SIZE	256
char msg[BUF_SIZE] = { MSG };

void setup() {
  P.begin();
  P.displayText(msg, scrollAlign, scrollSpeed, scrollPause, scrollEffect, scrollEffect);
}

void loop() {
  if (P.displayAnimate()){
    P.displayReset();
  }
}

